package de.grogra.video.ui;

import javax.swing.Box;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import de.grogra.video.export.VideoSettings;

/**
 * {@code AdvancedSettings} is a little {@link JFrame} which provides additional
 * settings to the plugin user interface. If the plugin will be extended by new
 * settings in the future, this class is the right place to set them.
 * 
 * @author Dominick Leppich
 *
 */
public class AdvancedSettings extends JFrame {
	private static final long serialVersionUID = 1L;

	private VideoSettings settings;

	private SpinnerNumberModel outputFramesModel;

	// ------------------------------------------------------------

	/**
	 * Create the frame for the advanced settings.
	 * 
	 * @param parent
	 *            - Parent {@link ExportPanel} object
	 * @param settings
	 *            - Reference to the corresponding {@link VideoSettings}
	 */
	public AdvancedSettings(ExportPanel parent, VideoSettings settings) {
		super("Advanced Settings");

		setLocationRelativeTo(parent);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);

		this.settings = settings;

		Box hBox = Box.createHorizontalBox();
		hBox.add(Box.createHorizontalStrut(10));
		Box vBox = Box.createVerticalBox();
		vBox.add(Box.createVerticalStrut(10));

		Box outputFpsBox = Box.createHorizontalBox();
		outputFpsBox.add(new JLabel("Output FPS:"));
		outputFpsBox.add(Box.createHorizontalStrut(5));
		outputFramesModel = new SpinnerNumberModel(settings.getOutputFps(), VideoSettings.OUTPUT_FPS_MIN,
				VideoSettings.FPS_MAX, 1);
		outputFramesModel.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				AdvancedSettings.this.settings.setOutputFps(outputFramesModel.getNumber().intValue());
			}
		});
		outputFpsBox.add(new JSpinner(outputFramesModel));
		vBox.add(outputFpsBox);

		vBox.add(Box.createVerticalStrut(10));
		hBox.add(vBox);
		hBox.add(Box.createHorizontalStrut(10));

		add(hBox);

		pack();
	}
}
