package de.grogra.video.ui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashSet;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.Set;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;

import de.grogra.video.connector.VideoPluginConnector;
import de.grogra.video.model.ImageSequence;
import de.grogra.video.model.ImageSequenceControl;
import de.grogra.video.model.ImageSequenceView;
import de.grogra.video.render.AutoRenderJob;
import de.grogra.video.render.ImageProvider;
import de.grogra.video.render.RenderDimension;
import de.grogra.video.render.RenderJob;
import de.grogra.video.simulation.SimulationMethod;
import de.grogra.video.util.Worker;

/**
 * This {@code RenderPanel} controls the rendering process.
 * 
 * @author Dominick Leppich
 *
 */
public class RenderPanel extends JPanel implements Observer, InteractionPanel {
	private static final long serialVersionUID = 1L;

	private Worker worker;

	private VideoPluginConnector connector;
	private ImageSequenceView view;
	private ImageSequenceControl control;
	private JComboBox<ImageProvider> providers;
	private JComboBox<RenderDimension> dimensions;
	private JComboBox<SimulationMethod> methods;
	private JSpinner countSpinner;
	private JButton renderButton;
	private JButton autoRenderButton;

	private boolean dimensionsEnabled;
	private Set<JComponent> components;

	// ------------------------------------------------------------

	/**
	 * Default ctr
	 * 
	 * @param worker
	 *            - {@link Worker} to use for the export
	 * @param connector
	 *            - {@link VideoPluginConnector}
	 * @param view
	 *            - {@link ImageSequenceView} to get reading access to the
	 *            {@link ImageSequence}
	 * @param control
	 *            - {@link ImageSequenceControl} to get writing access to the
	 *            {@link ImageSequence}
	 */
	public RenderPanel(Worker worker, VideoPluginConnector connector, ImageSequenceView view,
			ImageSequenceControl control) {
		/*
		 * Creating swing objects and positioning them on the panel using some layout as
		 * well as adding some simple listeners to them won't be explained in detail.
		 */
		this.worker = worker;
		this.connector = connector;
		this.view = view;
		this.control = control;

		dimensionsEnabled = true;

		components = new HashSet<>();

		setBorder(BorderFactory.createTitledBorder("Image rendering"));

		// Vertical box of this panel
		Box vBox = Box.createVerticalBox();

		// Box with all render buttons
		Box renderBox = Box.createHorizontalBox();
		JLabel renderLabel = new JLabel("Renderer:");
		components.add(renderLabel);
		renderBox.add(renderLabel);
		renderBox.add(Box.createHorizontalStrut(5));
		providers = createRendererSelectionBox();
		components.add(providers);
		renderBox.add(providers);
		renderBox.add(Box.createHorizontalStrut(5));
		JLabel dimensionLabel = new JLabel("Image dimension:");
		components.add(dimensionLabel);
		renderBox.add(dimensionLabel);
		renderBox.add(Box.createHorizontalStrut(5));
		dimensions = createDimensionSelectionBox();
		components.add(dimensions);
		renderBox.add(dimensions);
		renderBox.add(Box.createHorizontalStrut(5));
		renderButton = new JButton("Render");
		renderButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				renderImage();
			}
		});
		components.add(renderButton);
		renderBox.add(renderButton);
		vBox.add(renderBox);

		methods = createSimulationMethodSelectionBox();
		if (methods != null) {
			Box simulationBox = Box.createHorizontalBox();
			JLabel methodLabel = new JLabel("Simulation Method:");
			components.add(methodLabel);
			simulationBox.add(methodLabel);
			simulationBox.add(Box.createHorizontalStrut(5));
			components.add(methods);
			simulationBox.add(methods);
			simulationBox.add(Box.createHorizontalStrut(5));
			JLabel stepsLabel = new JLabel("Steps:");
			components.add(stepsLabel);
			simulationBox.add(stepsLabel);
			simulationBox.add(Box.createHorizontalStrut(5));
			SpinnerModel model = new SpinnerNumberModel(1, 1, 99999999, 1);
			countSpinner = new JSpinner(model);
			components.add(countSpinner);
			simulationBox.add(countSpinner);
			simulationBox.add(Box.createHorizontalStrut(5));
			autoRenderButton = new JButton("Auto-Render");
			autoRenderButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					autoRender();
				}
			});
			components.add(autoRenderButton);
			simulationBox.add(autoRenderButton);
			vBox.add(Box.createVerticalStrut(5));
			vBox.add(simulationBox);
		}

		add(vBox);
	}

	// ------------------------------------------------------------

	/**
	 * Create a {@link JComboBox} containing all available {@link ImageProvider}s.
	 * 
	 * @return {@link JComboBox} of {@link ImageProvider}s
	 */
	private JComboBox<ImageProvider> createRendererSelectionBox() {
		List<ImageProvider> providerList = connector.getImageProviders();

		ImageProvider[] providers = new ImageProvider[providerList.size()];
		providerList.toArray(providers);
		JComboBox<ImageProvider> providerBox = new JComboBox<>(providers);
		if (providerList.size() > 0)
			providerBox.setSelectedIndex(0);
		else
			providerBox.setEnabled(false);
		return providerBox;
	}

	/**
	 * Create a {@link JComboBox} containing all available {@link RenderDimension}s.
	 * 
	 * @return {@link JComboBox} of {@link RenderDimension}s
	 */
	private JComboBox<RenderDimension> createDimensionSelectionBox() {
		JComboBox<RenderDimension> dimensions = new JComboBox<>(RenderDimension.getDefaultRenderDimensions());
		dimensions.setSelectedIndex(14);
		return dimensions;
	}

	/**
	 * Create a {@link JComboBox} containing all available
	 * {@link SimulationMethod}s.
	 * 
	 * @return {@link JComboBox} of {@link SimulationMethod}s
	 */
	private JComboBox<SimulationMethod> createSimulationMethodSelectionBox() {
		List<SimulationMethod> methodList = connector.getSimulationMethods();

		if (methodList == null || methodList.isEmpty())
			return null;

		SimulationMethod[] methods = new SimulationMethod[methodList.size()];
		methodList.toArray(methods);
		JComboBox<SimulationMethod> methodBox = new JComboBox<>(methods);
		methodBox.setSelectedIndex(0);
		return methodBox;
	}

	// ------------------------------------------------------------

	/**
	 * Start the image rendering by creating a {@link RenderJob} and pass it to the
	 * {@link Worker}.
	 */
	private void renderImage() {
		worker.addJob(new RenderJob(providers.getItemAt(providers.getSelectedIndex()),
				dimensions.getItemAt(dimensions.getSelectedIndex()), control));
	}

	/**
	 * Start the auto image rendering by creating an {@link AutoRenderJob} and pass
	 * it to the {@link Worker}.
	 */
	private void autoRender() {
		int n = ((SpinnerNumberModel) countSpinner.getModel()).getNumber().intValue();
		worker.addJob(new AutoRenderJob(providers.getItemAt(providers.getSelectedIndex()),
				dimensions.getItemAt(dimensions.getSelectedIndex()), methods.getItemAt(methods.getSelectedIndex()),
				control, n));
	}

	/**
	 * Update the user interface control elements.
	 */
	private void updateControls() {
		dimensions.setEnabled(dimensionsEnabled);
	}

	// ------------------------------------------------------------

	/**
	 * Update this panel on changes of the {@link ImageSequence}.
	 */
	@Override
	public void update(Observable o, Object arg) {
		if (o instanceof ImageSequence) {
			// Disable render settings if image sequence contains at least one element
			// (render dimension cannot change...)
			dimensionsEnabled = view.isEmpty();
			updateControls();
		}
	}

	@Override
	public void setInteractionEnabled(boolean enabled) {
		this.setEnabled(enabled);
		for (JComponent c : components)
			c.setEnabled(enabled);
		if (enabled)
			updateControls();
	}
}
