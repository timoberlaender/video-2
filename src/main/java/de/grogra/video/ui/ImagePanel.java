package de.grogra.video.ui;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.geom.RoundRectangle2D;
import java.awt.image.BufferedImage;

import javax.swing.JPanel;

/**
 * Small class to display images in a panel with a fixed size. Images can be
 * drawn using the {@link #draw(Image) draw(Image)} method. The {@link #clear()
 * clear()} method will clear the image displaying area.
 * 
 * @author Dominick Leppich
 *
 */
public class ImagePanel extends JPanel {
	private static final long serialVersionUID = 1L;

	public static final int BORDER_WIDTH = (int) VideoPanel.IMAGE_PREVIEW_SIZE.getWidth();
	public static final int BORDER_HEIGHT = 10;
	public static final int BORDER_SIZE = 10;

	/**
	 * Size to display the image
	 */
	private final Dimension size;

	/**
	 * Image to display
	 */
	private Image image;
	private int current;
	private int max;
	/**
	 * Border for animation
	 */
	private boolean border;
	private Image borderImage;

	/**
	 * Create a new {@code ImagePanel} which displays an image with the given size.
	 * 
	 * @param width
	 *            - Width of the image to display
	 * @param height
	 *            - Height of the image to display
	 */
	public ImagePanel(int width, int height) {
		this.size = new Dimension(width, height);
		this.border = false;
		setPreferredSize(this.size);
	}

	// ------------------------------------------------------------

	/**
	 * Draws an image in the ImagePanel component. Bigger images will be down
	 * scaled, smaller ones will be displayed in the center.
	 * 
	 * @param img
	 *            - Image to draw
	 */
	public void draw(Image img, int current, int max) {
		this.image = img;
		this.current = current;
		this.max = max;
		repaint();
	}

	/**
	 * Clears the ImagePanel displaying area with a black color.
	 */
	public void clear() {
		image = null;
		current = 0;
		max = 0;
		repaint();
	}

	/**
	 * Enable or disable border
	 * 
	 * @param value
	 *            - Value
	 */
	public void setBorder(boolean value) {
		this.border = value;
		repaint();
	}

	// ------------------------------------------------------------

	/**
	 * Generates a preview border image.
	 * 
	 * @return A video preview border image
	 */
	private Image getBorderImage() {
		if (borderImage == null) {
			BufferedImage tmpImage = new BufferedImage(BORDER_WIDTH, BORDER_HEIGHT, BufferedImage.TYPE_INT_RGB);
			Graphics2D g = (Graphics2D) tmpImage.getGraphics();
			g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
			int i = -10;
			boolean color = true;
			while (i < BORDER_WIDTH) {
				if (color)
					g.setColor(Color.BLACK);
				else
					g.setColor(Color.WHITE);

				g.fillPolygon(new int[] { i, i + BORDER_SIZE, i + BORDER_SIZE + BORDER_HEIGHT, i + BORDER_SIZE },
						new int[] { 0, 0, BORDER_HEIGHT, BORDER_HEIGHT }, 4);

				color = !color;
				i += BORDER_SIZE;
			}
			borderImage = tmpImage;
		}
		return borderImage;
	}

	/**
	 * Paint a preview progress bar inside the {@code ImagePanel}.
	 * 
	 * @param g
	 *            - {@link Graphics2D} context
	 * @param x
	 *            - X position
	 * @param y
	 *            - Y position
	 * @param w
	 *            - Width
	 * @param h
	 *            - Height
	 */
	private void paintPreviewProgress(Graphics2D g, int x, int y, int w, int h) {
		RoundRectangle2D fullRect = new RoundRectangle2D.Double(x, y, w, h, 5, 5);
		RoundRectangle2D activeRect = new RoundRectangle2D.Double(x, y, (int) ((double) w * current / max), h, 5, 5);
		g.setColor(Color.BLACK);
		g.fill(fullRect);
		g.setColor(Color.WHITE);
		g.draw(fullRect);
		g.fill(activeRect);
	}

	/**
	 * Get the title.
	 * 
	 * @return Title
	 */
	private String getTitle() {
		if (current > 0 && max > 0)
			return "Image " + current + " of " + max;
		else
			return "--- no image ---";
	}

	/**
	 * Create an {@link AlphaComposite} object used for the transparent progress
	 * bar.
	 * 
	 * Source: http://www.informit.com/articles/article.aspx?p=26349&seqNum=5
	 * 
	 * @param alpha
	 *            - Transparence value
	 * @return {@link AlphaComposite}
	 */
	private AlphaComposite makeComposite(float alpha) {
		int type = AlphaComposite.SRC_OVER;
		return (AlphaComposite.getInstance(type, alpha));
	}

	// ------------------------------------------------------------

	/**
	 * Draw the image on the panel using the {@link java.awt.Graphics} context.
	 * Additionally draw preview borders, title or a progress bar.
	 * 
	 * @param g
	 *            - Graphics context
	 */
	@Override
	public void paintComponent(Graphics g) {
		Graphics2D g2d = (Graphics2D) g;

		// Enable antialiasing
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

		// Black background
		g2d.setColor(Color.BLACK);
		g2d.fillRect(0, 0, size.width, size.height);

		if (image != null) {

			// Draw image
			g2d.drawImage(image, 0, 0, null);

			// Draw borders if enabled
			if (border) {
				g2d.setComposite(makeComposite(0.5f));
				paintPreviewProgress(g2d, 10, size.height - 20 - BORDER_HEIGHT, size.width - 20, 10);

				g2d.setComposite(makeComposite(1.0f));
				g2d.drawImage(getBorderImage(), 0, 0, null);
				g2d.drawImage(getBorderImage(), 0, size.height - BORDER_HEIGHT, null);
			}
		}

		// else print title
		if (!border) {
			g2d.setColor(Color.WHITE);
			g2d.drawString(getTitle(), 10, 20);
		}
	}
}
