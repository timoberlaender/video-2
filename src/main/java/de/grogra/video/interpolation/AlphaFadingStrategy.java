package de.grogra.video.interpolation;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import de.grogra.video.model.VideoImage;

/**
 * Generates images using a simple alpha fading strategy. 
 * 
 * @author Dominick Leppich
 *
 */
public class AlphaFadingStrategy extends SimpleInterpolationStrategy {
	@Override
	protected List<VideoImage> computeImages(VideoImage a, VideoImage b, int n) throws IOException {
		Dimension dimension = a.getDimension();

		int width = dimension.width;
		int height = dimension.height;

		BufferedImage aImage = a.getImage();
		BufferedImage bImage = b.getImage();
		List<VideoImage> images = new ArrayList<>(n);

		// we want only alpha images between the original ones
		int denominator = n + 2;
		int nominator;

		// Notify
		setSubProgress(0.0);

		// Generate n images
		for (int i = 0; i < n; i++) {
			nominator = i + 1;
			double frac = (double) nominator / denominator;
			double ifrac = 1.0 - frac;

			// Create a new BufferedImage (to draw on)
			BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);

			// Generate the i-th image by calculating each pixel color
			for (int x = 0; x < width; x++) {
				for (int y = 0; y < height; y++) {
					Color aColor = new Color(aImage.getRGB(x, y));
					Color bColor = new Color(bImage.getRGB(x, y));
					Color newColor = new Color((int) (aColor.getRed() * ifrac + bColor.getRed() * frac),
							(int) (aColor.getGreen() * ifrac + bColor.getGreen() * frac),
							(int) (aColor.getBlue() * ifrac + bColor.getBlue() * frac));
					image.setRGB(x, y, newColor.getRGB());
				}
			}

			// Add image to list
			images.add(new VideoImage(image));

			// Notify
			setSubProgress((double) (i + 1) / n);
		}
		return images;
	}

	@Override
	public String getName() {
		return "AlphaFading";
	}
}
