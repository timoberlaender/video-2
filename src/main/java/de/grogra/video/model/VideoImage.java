package de.grogra.video.model;

import java.awt.Dimension;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Random;

import javax.imageio.ImageIO;

import de.grogra.video.ui.VideoPanel;

/**
 * A {@link VideoImage} class is a wrapper for images in the plugin. Every image
 * is saved to a temporary directory on the filesystem to be accessible for the
 * exporter at video creation time.
 * 
 * Additionally to the normal image, a scaled preview version is saved as well.
 * 
 * @author Dominick Leppich
 *
 */
public class VideoImage {
	public static final String IMAGE_TYPE = "png";
	public static final String IMAGE_EXTENSION = ".png";
	private static int imageId = 1;
	private static File savePath;

	// ------------------------------------------------------------

	private File imageFile;
	private final File previewImageFile;
	private final Dimension dimension;

	// ------------------------------------------------------------

	/**
	 * Create a new {@link VideoImage}.
	 * 
	 * @param image
	 *            - Image to wrap
	 * @throws IOException
	 *             if saving the image on the filesystem fails
	 */
	public VideoImage(Image image) throws IOException {
		imageFile = new File(getSavePath().getAbsolutePath() + File.separator + imageId + IMAGE_EXTENSION);
		previewImageFile = new File(
				getSavePath().getAbsolutePath() + File.separator + "pre_" + imageId + IMAGE_EXTENSION);
		imageId++;

		save(image, imageFile);
		save(createPreviewImage(image), previewImageFile);

		dimension = new Dimension(image.getWidth(null), image.getHeight(null));
	}

	// ------------------------------------------------------------
	/* Getter and Setter */
	
	public BufferedImage getImage() throws IOException {
		File f = getFile();
		if (!f.exists())
			throw new IOException("VideoImage \"" + f.getAbsolutePath() + "\" not found");
		return ImageIO.read(f);
	}

	public Image getPreviewImage() throws IOException {
		File f = getPreviewFile();
		if (!f.exists())
			throw new IOException("VideoImage \"" + f.getAbsolutePath() + "\" not found");
		return ImageIO.read(f);
	}

	public File getFile() {
		return imageFile;
	}

	private File getPreviewFile() {
		return previewImageFile;
	}

	public Dimension getDimension() {
		return dimension;
	}

	// ------------------------------------------------------------

	/**
	 * Rename the image file.
	 * 
	 * @param prefix
	 *            - New prefix
	 * @param number
	 *            - New number
	 */
	void rename(String prefix, int number) {
		File oldFile = imageFile;
		imageFile = new File(getSavePath() + File.separator + prefix + "_" + number + IMAGE_EXTENSION);
		oldFile.renameTo(imageFile);
		imageFile.deleteOnExit();
	}

	// ------------------------------------------------------------

	/**
	 * Save an image and mark it for removal after programm termination.
	 * 
	 * @param image
	 *            - Image to save
	 * @param file
	 *            - Output file
	 * @throws IOException
	 *             if saving fails
	 */
	private static void save(Image image, File file) throws IOException {
		ImageIO.write(convertToBufferedImage(image), IMAGE_TYPE, file);
		file.deleteOnExit();
	}

	/**
	 * Determine the save path for the running plugin instance.
	 * 
	 * @return Path where the {@link VideoImage}s will be saved
	 */
	public synchronized static File getSavePath() {
		// Create on first access
		if (savePath == null) {
			do {
				savePath = new File(
						getSystemTempPath() + File.separator + "GroIMP-VideoPlugin-" + new Random().nextInt(10000000));
			} while (savePath.exists());
			savePath.mkdir();
			savePath.deleteOnExit();
		}
		return savePath;
	}

	/**
	 * Determine the path of temporary system directory
	 * 
	 * @return Path of temporary system directory
	 */
	public static String getSystemTempPath() {
		return System.getProperty("java.io.tmpdir");
	}

	/**
	 * Create a preview image with predefined dimensions.
	 * 
	 * @param image
	 *            - Source image
	 * @return Scaled preview image
	 */
	public static Image createPreviewImage(Image image) {
		Dimension previewDimension = VideoPanel.IMAGE_PREVIEW_SIZE;
		return image.getScaledInstance(previewDimension.width, previewDimension.height, Image.SCALE_SMOOTH);
	}

	/**
	 * Convert an {@link Image} to a {@link BufferedImage} in order to save it with
	 * {@link ImageIO#write(java.awt.image.RenderedImage, String, File)}.
	 * 
	 * @param image
	 *            - Image to convert
	 * @return A {@link BufferedImage} containing the same image information
	 */
	public static BufferedImage convertToBufferedImage(Image image) {
		BufferedImage bufferedImage = new BufferedImage(image.getWidth(null), image.getHeight(null),
				BufferedImage.TYPE_INT_RGB);
		bufferedImage.getGraphics().drawImage(image, 0, 0, null);
		return bufferedImage;
	}
}
