package de.grogra.video.model;

import java.awt.Dimension;
import java.util.List;

/**
 * Viewer for an {@link ImageSequence} which grants read only access to the
 * sequence.
 * 
 * @author Dominick Leppich
 *
 */
public interface ImageSequenceView {
	/**
	 * Check, if a given {@link Object} is inside this sequence.
	 * 
	 * @param obj
	 *            - {@link Object} to check
	 * @return True, if this sequence contains the specified {@link Object}
	 */
	boolean contains(Object arg0);

	/**
	 * Get the {@link VideoImage} at a given position.
	 * 
	 * @param index
	 *            - Index to get the image from
	 * @return The {@link VideoImage} at the given position
	 */
	VideoImage get(int arg0);

	/**
	 * Get the whole sequence of {@link VideoImage}s as a {@link List}.
	 * 
	 * @return All {@link VideoImage}s in a {@link List}
	 */
	List<VideoImage> getList();

	/**
	 * Check, if the sequence is empty.
	 * 
	 * @return True, if the sequence is empty
	 */
	boolean isEmpty();

	/**
	 * Return the size of the sequence (the number of {@link VideoImage}s saved in
	 * the sequence).
	 * 
	 * @return The size of the sequence
	 */
	int size();

	/**
	 * Return the {@link Dimension} of the images in this sequence. This dimension
	 * is unique, because the sequence doesn't allow to add images with mismatching
	 * dimensions.
	 * 
	 * @return {@link VideoImage} dimension of this sequence
	 */
	Dimension getDimension();

	/**
	 * Renames all files and create a regular expression for all files.
	 */
	void reorder();

	/**
	 * Get the regular expression describing all image files.
	 * 
	 * @return Regular expression
	 */
	String getRegex();
}
