package de.grogra.video.model;

import java.awt.Dimension;
import java.io.File;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Observable;
import java.util.Random;

/**
 * An {@code ImageSequence} is able to collect an ordered list of images. Read
 * and write operations on this list are possible through
 * {@link ImageSequenceView} and {@link ImageSequenceControl} interfaces. The
 * sequence is observerable and can notify observers about
 * {@link ImageSequenceEvent}s, which are triggered by modifications of the
 * sequence. The sequence manages a list of {@link VideoImage}s and uses a
 * {@link LinkedList} internally.
 * 
 * @author Dominick Leppich
 *
 */
public class ImageSequence extends Observable {
	public static final char[] ALPHABET = new char[] { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
			'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
			'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' };

	/**
	 * Ordered list of collected images
	 */
	private List<VideoImage> images = new LinkedList<>();
	/**
	 * Dimension of the collected images
	 */
	private Dimension dimension;

	private ImageSequenceView view;
	private ImageSequenceControl control;

	private String prefix;

	// ------------------------------------------------------------

	/**
	 * Add a {@link VideoImage} at the end of the sequence.
	 * 
	 * @param image
	 *            - {@link VideoImage}
	 */
	public void add(VideoImage image) {
		add(size(), image);
	}

	/**
	 * Add a {@link VideoImage} at a specified position in the sequence.
	 * 
	 * @param index
	 *            - Index to insert the image
	 * @param image
	 *            - {@link VideoImage}
	 */
	public synchronized void add(int index, VideoImage image) {
		Dimension imageDimension = image.getDimension();
		if (dimension == null)
			dimension = imageDimension;
		else if (!dimension.equals(imageDimension))
			throw new IllegalArgumentException("Image dimensions mismatch! Sequence dimension: " + dimension
					+ ", Image dimension: " + imageDimension);
		images.add(index, image);
		// Change
		changed(ImageSequenceEvent.IMAGE_ADDED, index);
	}

	/**
	 * Clear the sequence of images.
	 */
	public synchronized void clear() {
		images.clear();
		reset();
		// Change
		changed(ImageSequenceEvent.IMAGE_REMOVED, -1);
	}

	/**
	 * Check, if a given {@link Object} is inside this sequence.
	 * 
	 * @param obj
	 *            - {@link Object} to check
	 * @return True, if this sequence contains the specified {@link Object}
	 */
	public synchronized boolean contains(Object obj) {
		return images.contains(obj);
	}

	/**
	 * Get the {@link VideoImage} at a given position.
	 * 
	 * @param index
	 *            - Index to get the image from
	 * @return The {@link VideoImage} at the given position
	 */
	public synchronized VideoImage get(int index) {
		return images.get(index);
	}

	/**
	 * Get the whole sequence of {@link VideoImage}s as a {@link List}.
	 * 
	 * @return All {@link VideoImage}s in a {@link List}
	 */
	public synchronized List<VideoImage> getList() {
		return images;
	}

	/**
	 * Check, if the sequence is empty.
	 * 
	 * @return True, if the sequence is empty
	 */
	public synchronized boolean isEmpty() {
		return images.isEmpty();
	}

	/**
	 * Remove the image at the specified position.
	 * 
	 * @param index
	 *            - Index of the image to remove
	 * @return The {@link VideoImage} which was removed
	 */
	public synchronized VideoImage remove(int index) {
		VideoImage result = images.remove(index);
		// Change
		changed(ImageSequenceEvent.IMAGE_REMOVED, index);
		if (images.isEmpty())
			reset();
		return result;
	}

	/**
	 * Return the size of the sequence (the number of {@link VideoImage}s saved in
	 * the sequence).
	 * 
	 * @return The size of the sequence
	 */
	public synchronized int size() {
		return images.size();
	}

	/**
	 * Return the {@link Dimension} of the images in this sequence. This dimension
	 * is unique, because the sequence doesn't allow to add images with mismatching
	 * dimensions.
	 * 
	 * @return {@link VideoImage} dimension of this sequence
	 */
	public synchronized Dimension getDimension() {
		return new Dimension(dimension);
	}

	/**
	 * Renames all files and create a regular expression for all files.
	 */
	public synchronized void reorder() {
		// Determine used prefixes
		HashSet<String> usedPrefixes = new HashSet<>();
		for (File f : VideoImage.getSavePath().listFiles()) {
			String[] split = f.getName().split("_");
			if (split.length > 1) {
				String p = f.getName().split("_")[0];
				if (!usedPrefixes.contains(p))
					usedPrefixes.add(p);
			}
		}

		// Calculate new prefix
		Random rnd = new Random(System.nanoTime());
		do {
			prefix = "";
			for (int i = 0; i < 5; i++)
				prefix += ALPHABET[rnd.nextInt(ALPHABET.length)];
		} while (usedPrefixes.contains(prefix));

		// Rename all images
		for (int i = 0; i < size(); i++)
			get(i).rename(prefix, i + 1);
	}

	/**
	 * Get the regular expression describing all image files.
	 * 
	 * @return Regular expression
	 */
	public synchronized String getRegex() {
		return VideoImage.getSavePath() + File.separator + prefix + "_%d" + VideoImage.IMAGE_EXTENSION;
	}

	// ------------------------------------------------------------

	/**
	 * Get an {@link ImageSequenceView} for this sequence. This method will only
	 * create one instance.
	 * 
	 * @return An {@link ImageSequenceView}
	 */
	public synchronized ImageSequenceView view() {
		if (view == null)
			view = new ImageSequenceViewer(this);
		return view;
	}

	/**
	 * Get an {@link ImageSequenceControl} for this sequence. This method will only
	 * create one instance.
	 * 
	 * @return An {@link ImageSequenceControl}
	 */
	public synchronized ImageSequenceControl control() {
		if (control == null)
			control = new ImageSequenceController(this);
		return control;
	}

	// ------------------------------------------------------------

	/**
	 * This method is called whenever the state of the model (the list) changes.
	 * 
	 * @param type
	 *            - The type of change (defined in the class
	 *            {@link ImageSequenceEvent})
	 * @param index
	 *            - the index where the change occured
	 */
	private void changed(int type, int index) {
		ImageSequenceEvent event = new ImageSequenceEvent(type, index, size());
		setChanged();
		notifyObservers(event);
	}

	/**
	 * Reset the sequence.
	 */
	private void reset() {
		dimension = null;
	}
}
