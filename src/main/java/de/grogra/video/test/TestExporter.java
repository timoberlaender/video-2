package de.grogra.video.test;

import java.io.File;
import java.util.LinkedList;
import java.util.List;

import de.grogra.video.export.FileFormat;
import de.grogra.video.export.VideoExporter;
import de.grogra.video.export.VideoSettings;
import de.grogra.video.model.ImageSequenceView;
import de.grogra.video.model.VideoImage;
import de.grogra.video.util.Progress;

/**
 * This test implementation of the abstract {@link VideoExporter} class just
 * prints the filenames of all images in the {@link ImageSequence} to export.
 * This exporter has no available {@link FileFormat}s.
 * 
 * @author Dominick Leppich
 *
 */
public class TestExporter extends VideoExporter {

	@Override
	public void createVideo(ImageSequenceView view, VideoSettings settings, File fileName) {
		setProgress(new Progress(0.0, "Exporting Video: TestExporter"));
		for (int i = 0; i < view.size(); i++) {
			VideoImage image = view.get(i);
			System.out.println("Exporting image: " + image.getFile().getAbsolutePath());
			setProgress(new Progress((double) (i + 1) / view.size(), "Exporting Video: TestExporter"));
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public List<FileFormat> getFileFormats() {
		List<FileFormat> formats = new LinkedList<>();

		return formats;
	}

}
