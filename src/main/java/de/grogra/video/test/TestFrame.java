package de.grogra.video.test;

import javax.swing.JFrame;

import de.grogra.video.ui.VideoPanel;

/**
 * This {@code TestFrame} wraps a {@link VideoPanel} into a swing
 * {@link JFrame}. This class can be used to test the graphical user interface
 * of the plugin without GroIMP.
 * 
 * @author Dominick Leppich
 *
 */
public class TestFrame extends JFrame {
	private static final long serialVersionUID = 1L;
	
	// ------------------------------------------------------------

	/**
	 * Ctr.
	 * 
	 * @param videoPanel
	 *            - {@link VideoPanel} to wrap
	 */
	public TestFrame(VideoPanel videoPanel) {
		add(videoPanel);
		pack();
		setResizable(false);
		setVisible(true);
	}
}
