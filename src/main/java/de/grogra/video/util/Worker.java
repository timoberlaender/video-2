package de.grogra.video.util;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;

/**
 * The {@code Worker} is used to process jobs inside the plugin. Although the
 * real processing is done inside a corresponding {@link WorkerThread}, the
 * plugin communicates the the {@code Worker} only. To program lifetime there
 * can only be one instance of this class, which can be created using the
 * {@link #instance()} method. An arbitrary amount of {@link JobListener}s can
 * be add to this {@code Worker}. Jobs can be added to the {@code Worker} using
 * the {@link #addJob(Job)} method. To make sure the program doesn't continue
 * until a job has finished, you can call the {@link #waitUntilFinished()}
 * method.
 * 
 * @author Dominick Leppich
 *
 */
public class Worker extends ProgressObservable {
	private static Worker instance;

	/**
	 * {@link WorkerThread} used for processing the jobs
	 */
	private WorkerThread workerThread;
	/**
	 * {@link Queue} of queued jobs
	 */
	private Queue<Job> jobs;
	/**
	 * The {@link Set} of registered {@link JobListener}s to this {@code Worker}
	 */
	private Set<JobListener> listener;

	// ------------------------------------------------------------

	/**
	 * Private ctr. Initializes all member variables.
	 */
	private Worker() {
		jobs = new LinkedList<>();
		listener = new HashSet<>();
		initThread();
	}

	/**
	 * Initialize the {@link WorkerThread}.
	 */
	private void initThread() {
		// Create a new worker thread and set itself as the corresponding worker
		workerThread = new WorkerThread(this);
		// Set the thread as a daemon (to stop automatically if no other non-daemon
		// thread is left)
		workerThread.setDaemon(true);
		// Start the worker thread
		workerThread.start();
	}

	/**
	 * Get (and create on first call) an instance of the {@link Worker} class.
	 * 
	 * @return A {@link Worker} instance
	 */
	public synchronized static Worker instance() {
		if (instance == null)
			instance = new Worker();
		return instance;
	}

	// ------------------------------------------------------------

	/**
	 * Add a {@link JobListener} to the {@code Worker}.
	 * 
	 * @param listener
	 *            - {@link JobListener} to add
	 */
	public void addJobListener(JobListener listener) {
		this.listener.add(listener);
	}

	/**
	 * Removes a {@link JobListener} from the {@code Worker}.
	 * 
	 * @param listener
	 *            - {@link JobListener} to remove
	 */
	public void removeJobListener(JobListener listener) {
		this.listener.remove(listener);
	}

	// ------------------------------------------------------------

	/**
	 * Add a new {@link Job} to the worker. The job is queued at the end.
	 * 
	 * @param job
	 *            - {@link Job} to add
	 * @return True, if adding the {@link Job} was successfull
	 */
	public boolean addJob(Job job) {
		boolean result = jobs.add(job);
		workerThread.wakeUp();
		return result;
	}

	/**
	 * Get the next {@link Job} to process. This method is called by the
	 * {@link WorkerThread}. The returned {@link Job} is removed from the job queue.
	 * 
	 * @return Next {@link Job} to process
	 */
	Job getNextJob() {
		return jobs.poll();
	}

	/**
	 * Check if there are queued jobs.
	 * 
	 * @return True, if the worker has queued jobs
	 */
	public boolean hasJobs() {
		return !jobs.isEmpty();
	}

	/**
	 * Let the calling thread wait, until the worker finishes processing all jobs.
	 */
	public synchronized void waitUntilFinished() {
		try {
			wait();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Cancel the job processing. The current processed {@link Job} is lost.
	 * 
	 * @throws InterruptedException
	 *             if the {@link WorkerThread} is interrupted
	 */
	public void cancel() throws InterruptedException {
		workerThread.interrupt();
		workerThread.join();
		initThread();
	}

	// ------------------------------------------------------------

	/**
	 * This method is called by the {@link WorkerThread}, if a new job is started to
	 * process. All {@link JobListener}s are notified.
	 * 
	 * @param job
	 *            - {@link Job} to be started to process
	 */
	void startJob(Job job) {
		for (JobListener jl : listener)
			jl.startJob(job);
	}

	/**
	 * This method is called by the {@link WorkerThread}, if a job is finished
	 * processing. All {@link JobListener}s are notified.
	 * 
	 * @param job
	 *            - {@link Job} finished to process
	 */
	void endJob(Job job) {
		for (JobListener jl : listener)
			jl.endJob(job);
	}

	/**
	 * This method is called by the {@link WorkerThread}, if all jobs has been
	 * finished. All {@link JobListener}s are notified.
	 */
	synchronized void finishedWork() {
		for (JobListener jl : listener)
			jl.finishedWork();
		notify();
	}
}
