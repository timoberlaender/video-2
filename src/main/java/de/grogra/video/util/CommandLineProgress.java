package de.grogra.video.util;

import java.util.Observable;
import java.util.Observer;

/**
 * The {@code CommandLineProgress} is an observer for objects, which are
 * {@link ProgressObservable}. This class prints the progress to the standard
 * output on the command line.
 * 
 * @author Dominick Leppich
 *
 */
public class CommandLineProgress implements Observer {
	private String title;
	private int width;
	private char charEmpty;
	private char charDone;
	private String leftBound;
	private String rightBound;
	private boolean showProgress;

	// ------------------------------------------------------------

	/**
	 * Create a {@code CommandLineProgress} object defining its style.
	 * 
	 * @param title
	 *            - Title of the {@code CommandLineProgress} (will be printed before
	 *            each progress output)
	 * @param width
	 *            - Number of characters representing the progress
	 * @param charEmpty
	 *            - Character for empty progress bar
	 * @param charDone
	 *            - Character for filled progress bar
	 * @param leftBound
	 *            - Left bound character of the progress bar
	 * @param rightBound
	 *            - Right bound character of the progress bar
	 * @param showProgress
	 *            - Show the progress value at the end
	 */
	public CommandLineProgress(String title, int width, char charEmpty, char charDone, String leftBound,
			String rightBound, boolean showProgress) {
		this.title = title;
		this.width = width;
		this.charEmpty = charEmpty;
		this.charDone = charDone;
		this.leftBound = leftBound;
		this.rightBound = rightBound;
		this.showProgress = showProgress;
	}

	// ------------------------------------------------------------

	@Override
	public synchronized void update(Observable o, Object arg) {
		if (o instanceof Job && arg instanceof Progress) {
			Progress p = (Progress) arg;

			StringBuilder s = new StringBuilder();

			if (title != null)
				s.append(title + "\t ");
			s.append(p.getName() + "\t ");
			if (leftBound != null)
				s.append(leftBound);

			int i;
			for (i = 0; i < width * p.getValue(); i++)
				s.append(charDone);
			for (; i < width; i++)
				s.append(charEmpty);

			if (rightBound != null)
				s.append(rightBound);

			if (showProgress)
				s.append(String.format(" %3d", (int) (100 * p.getValue())) + '%');

			System.out.println(s);
		}
	}
}
