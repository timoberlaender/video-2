package de.grogra.video.util;

/**
 * This {@code JobListener} defines methods, which are called by the
 * {@link Worker} when a job is started, ended and the worker finished
 * processing all jobs.
 * 
 * @author Dominick Leppich
 *
 */
public interface JobListener {
	/**
	 * The worker started processing this job.
	 * 
	 * @param job
	 *            - The job started to be processed
	 */
	void startJob(Job job);

	/**
	 * The worker finished processing this job.
	 * 
	 * @param job
	 *            - The job finished
	 */
	void endJob(Job job);

	/**
	 * The worker finished processing all jobs.
	 */
	void finishedWork();
}
