package de.grogra.video.util;

import de.grogra.video.VideoPlugin;

/**
 * The {@code WorkerThread} belongs to a {@link Worker} and is used to process
 * the jobs in a separate Thread.
 * 
 * @author Dominick Leppich
 *
 */
public class WorkerThread extends Thread {
	private Worker worker;
	private boolean working;

	// ------------------------------------------------------------

	/**
	 * Ctr.
	 * 
	 * @param worker
	 *            - The {@link Worker} this {@code WorkerThread} belongs to
	 */
	public WorkerThread(Worker worker) {
		this.worker = worker;
		setName("VideoPlugin WorkerThread");
	}

	// ------------------------------------------------------------

	/**
	 * This is the main processing loop of the worker. While it is working, it asks
	 * the corresponding {@link Worker} for new jobs and start processing them.
	 * Before the processing starts, it calls the listener method
	 * {@link JobListener#startJob(Job)}. After the job finished processing, the
	 * {@code WorkerThread} calls the listener method
	 * {@link JobListener#endJob(Job)}. This loop repeats until no jobs remain or
	 * the worker is interrupted from the outside. In both cases the listener method
	 * {@link JobListener#finishedWork()} is called. After finishing all jobs, the
	 * {@code WorkerThread} waits until new jobs arrive.
	 */
	public synchronized void run() {
		Job job = null;
		while (true) {
			try {
				working = true;
				if (worker.hasJobs()) {
					job = worker.getNextJob();
					worker.startJob(job);
					job.process();
					worker.endJob(job);
				} else {
					worker.finishedWork();
					working = false;
					wait();
				}
			} catch (InterruptedException e) {
				// Set current job as finished
				if (job != null)
					worker.endJob(job);
				worker.finishedWork();
				return;
			} catch (Exception | Error e) {
				VideoPlugin.handleError(e);
				// Set current job as finished
				if (job != null)
					worker.endJob(job);
			}
		}
	}

	/**
	 * The {@code wakeUp()} method is used to make the {@code WorkerThread} continue
	 * processing jobs, after adding a new job.
	 */
	void wakeUp() {
		if (!working)
			synchronized (this) {
				notify();
			}
	}
}
